-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2021 at 12:45 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bankmisr`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `number_of_players` int(11) DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `name`, `number_of_players`, `league_id`) VALUES
(17, 'group_0', 3, 1),
(18, 'group_1', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(23);

-- --------------------------------------------------------

--
-- Table structure for table `leagues`
--

CREATE TABLE `leagues` (
  `league_id` int(11) NOT NULL,
  `league_startdate` date DEFAULT NULL,
  `max_players` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `number_of_daily_matches` int(11) DEFAULT NULL,
  `league_champion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `leagues`
--

INSERT INTO `leagues` (`league_id`, `league_startdate`, `max_players`, `name`, `number_of_daily_matches`, `league_champion`) VALUES
(1, '2021-06-11', '12', 'league1', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `matche_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `result` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL,
  `player1` int(11) DEFAULT NULL,
  `player2` int(11) DEFAULT NULL,
  `round_id` int(11) DEFAULT NULL,
  `winner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`matche_id`, `date`, `result`, `league_id`, `player1`, `player2`, `round_id`, `winner`) VALUES
(0, '2021-06-11', NULL, 1, 1, 2, 1, NULL),
(1, '2021-06-11', NULL, 1, 1, 2, 1, NULL),
(2, '2021-06-16', NULL, 1, 3, 4, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `participant_id` int(11) NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`participant_id`, `gender`, `name`, `group_id`, `league_id`) VALUES
(1, 1, 'player1', 17, 1),
(2, 0, 'player2', 17, 1),
(3, 0, 'player3', 17, 1),
(4, 1, 'player4', 18, 1),
(5, 1, 'player5', 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rounds`
--

CREATE TABLE `rounds` (
  `round_id` int(11) NOT NULL,
  `closed` bit(1) DEFAULT NULL,
  `round_number` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL,
  `round_winner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `rounds`
--

INSERT INTO `rounds` (`round_id`, `closed`, `round_number`, `league_id`, `round_winner`) VALUES
(1, b'1', '1', 1, 2),
(20, b'0', '2', 1, NULL),
(21, b'0', '3', 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `FKf7dkexwgxrmqm917lqvamkru2` (`league_id`);

--
-- Indexes for table `leagues`
--
ALTER TABLE `leagues`
  ADD PRIMARY KEY (`league_id`),
  ADD KEY `FKdlg67y57bo8ygksv52p2comkx` (`league_champion`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`matche_id`),
  ADD KEY `FK23dnop04r2pfj2wvo21vakpph` (`league_id`),
  ADD KEY `FKmkph7wk92icve5opdsdjr8usq` (`player1`),
  ADD KEY `FK3fd0hvqt1tvy0hbktjupeickr` (`player2`),
  ADD KEY `FKjqepgamspeqo5q36vffq2h593` (`round_id`),
  ADD KEY `FKsbcljxw83jre5fci02f5cfw4n` (`winner`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`participant_id`),
  ADD KEY `FK211xf632vunjpcm01nrbaely8` (`group_id`),
  ADD KEY `FKpgd0sf1mulou4mokkyjv5lvu8` (`league_id`);

--
-- Indexes for table `rounds`
--
ALTER TABLE `rounds`
  ADD PRIMARY KEY (`round_id`),
  ADD KEY `FKq5ifljsrjprxgl0iyksw5qp52` (`league_id`),
  ADD KEY `FKnil24ynvmt3r618o6l271tw94` (`round_winner`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `FKf7dkexwgxrmqm917lqvamkru2` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`league_id`);

--
-- Constraints for table `leagues`
--
ALTER TABLE `leagues`
  ADD CONSTRAINT `FKdlg67y57bo8ygksv52p2comkx` FOREIGN KEY (`league_champion`) REFERENCES `participants` (`participant_id`);

--
-- Constraints for table `matches`
--
ALTER TABLE `matches`
  ADD CONSTRAINT `FK23dnop04r2pfj2wvo21vakpph` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`league_id`),
  ADD CONSTRAINT `FK3fd0hvqt1tvy0hbktjupeickr` FOREIGN KEY (`player2`) REFERENCES `participants` (`participant_id`),
  ADD CONSTRAINT `FKjqepgamspeqo5q36vffq2h593` FOREIGN KEY (`round_id`) REFERENCES `rounds` (`round_id`),
  ADD CONSTRAINT `FKmkph7wk92icve5opdsdjr8usq` FOREIGN KEY (`player1`) REFERENCES `participants` (`participant_id`),
  ADD CONSTRAINT `FKsbcljxw83jre5fci02f5cfw4n` FOREIGN KEY (`winner`) REFERENCES `participants` (`participant_id`);

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `FK211xf632vunjpcm01nrbaely8` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `FKpgd0sf1mulou4mokkyjv5lvu8` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`league_id`);

--
-- Constraints for table `rounds`
--
ALTER TABLE `rounds`
  ADD CONSTRAINT `FKnil24ynvmt3r618o6l271tw94` FOREIGN KEY (`round_winner`) REFERENCES `participants` (`participant_id`),
  ADD CONSTRAINT `FKq5ifljsrjprxgl0iyksw5qp52` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`league_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
