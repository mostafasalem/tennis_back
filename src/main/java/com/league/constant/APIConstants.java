package com.league.constant;


public class APIConstants {
	public static final String REGEX_FOR_NUMBERS = "^\\d+$";
	public static final String MESSAGE_FOR_ALL_ERRORS = "Internal Server Error";
	public static final String MESSAGE_FOR_ALL_EXCEPTIONS = "Internal Server Error";
	public static final String MESSAGE_FOR_REGEX_NUMBER_MISMATCH = "ID should contains integers only";
	public static final String MESSAGE_FOR_INVALID_PARAMETERS_ERROR = "Invalid Parameters";
	public static final String MESSAGE_FOR_INVALID_BODY_ERROR = "Invalid Method Body. Check JSON Objects";
	public static final String DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";
}
