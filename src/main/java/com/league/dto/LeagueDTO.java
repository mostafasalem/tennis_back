package com.league.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.springframework.stereotype.Component;

import com.league.model.Participant;



/**
* 
* @author Mostafa Salem
* 
*/


@Component
public class LeagueDTO implements Serializable {
	/** Version identifier for serialization. */
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String maxPlayers;
	private int numberOfDailyMatches;
	private int leagueChampion;
	private Date leagueStartDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(String maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public Integer getNumberOfDailyMatches() {
		return numberOfDailyMatches;
	}

	public void setNumberOfDailyMatches(Integer numberOfDailyMatches) {
		this.numberOfDailyMatches = numberOfDailyMatches;
	}

	public int getLeagueChampion() {
		return leagueChampion;
	}

	public void setLeagueChampion(int leagueChampion) {
		this.leagueChampion = leagueChampion;
	}

	public Date getLeagueStartDate() {
		return leagueStartDate;
	}

	public void setLeagueStartDate(Date leagueStartDate) {
		this.leagueStartDate = leagueStartDate;
	}


			
}
