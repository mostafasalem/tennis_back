package com.league.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.league.model.League;
import com.league.model.Participant;
import com.league.model.Round;

@Component
public class MatcheDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Date date;
	private Integer player1;
	private Integer player2;
	private Integer winner;
	private String result;
	
	public Integer getId() {
		return id;
	}
	public Integer getWinner() {
		return winner;
	}
	public void setWinner(Integer winner) {
		this.winner = winner;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getPlayer1() {
		return player1;
	}
	public void setPlayer1(Integer player1) {
		this.player1 = player1;
	}
	public Integer getPlayer2() {
		return player2;
	}
	public void setPlayer2(Integer player2) {
		this.player2 = player2;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	

	
	
	
}
