package com.league.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.league.model.Group;
import com.league.model.League;

@Component
public class ParticipantDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	// 0 : female
	// 1 : male
	private Integer gender;
	private Integer leagueId;
	
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}

	
}
