package com.league.dto;

import java.io.Serializable;

public class RoundDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String roundNumber;
	private boolean closed = false;
	private Integer leagueId;
	private Integer roundWinnerId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRoundNumber() {
		return roundNumber;
	}
	public void setRoundNumber(String roundNumber) {
		this.roundNumber = roundNumber;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	public Integer getRoundWinnerId() {
		return roundWinnerId;
	}
	public void setRoundWinnerId(Integer roundWinnerId) {
		this.roundWinnerId = roundWinnerId;
	}
	
}
