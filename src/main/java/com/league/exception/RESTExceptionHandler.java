package com.league.exception;

import static com.league.constant.APIConstants.MESSAGE_FOR_ALL_ERRORS;
import static com.league.constant.APIConstants.MESSAGE_FOR_ALL_EXCEPTIONS;
import static com.league.constant.APIConstants.MESSAGE_FOR_INVALID_BODY_ERROR;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RESTExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<Object> resourceEntityNotFound(ResourceNotFoundException e) {
		APIError apiError = createError(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST, e);
		e.printStackTrace(System.out);
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}

	@Override
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		APIError apiError = createError(MESSAGE_FOR_INVALID_BODY_ERROR, HttpStatus.BAD_REQUEST, e);
		e.printStackTrace(System.out);
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}

	// All compile time exceptions
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Object> handleAllExceptions(Exception e) {
		APIError apiError = createError(MESSAGE_FOR_ALL_ERRORS, HttpStatus.INTERNAL_SERVER_ERROR, e);
		e.printStackTrace(System.out);
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}

	// All runtime exceptions
	@ExceptionHandler({ RuntimeException.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Object> handleRunTimeException(RuntimeException e) {
		APIError apiError = createError(MESSAGE_FOR_ALL_EXCEPTIONS, HttpStatus.INTERNAL_SERVER_ERROR, e);
		e.printStackTrace(System.out);
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}

	private APIError createError(String msg, HttpStatus status, Exception e) {
		APIError apiError = new APIError(status);
		apiError.setMessage(msg);
		apiError.setDebugMessage(e.getMessage());
		return apiError;
	}
}
