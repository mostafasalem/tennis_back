package com.league.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;



/**
* 
* @author Mostafa Salem
* 
*/


@Entity
@Table(name = "leagues")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class League implements Serializable {
	/** Version identifier for serialization. */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "league_id")
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "max_players")
	private String maxPlayers;

	@Column(name = "number_of_daily_matches")
	private Integer numberOfDailyMatches;

	@Column(name = "leagueChampion")
	private Integer leagueChampion;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "league_startdate")
	private Date leagueStartDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "league", cascade = CascadeType.ALL)
	private List<Group> groups;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "league", cascade = CascadeType.ALL)
	private List<Round> rounds;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "league", cascade = CascadeType.ALL)
	private List<Matche> matches;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "league", cascade = CascadeType.ALL)
	private List<Participant> participants;


	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Round> getRounds() {
		return rounds;
	}

	public void setRounds(List<Round> rounds) {
		this.rounds = rounds;
	}

	public List<Matche> getMatches() {
		return matches;
	}

	public void setMatches(List<Matche> matches) {
		this.matches = matches;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(String maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public Integer getNumberOfDailyMatches() {
		return numberOfDailyMatches;
	}

	public void setNumberOfDailyMatches(Integer numberOfDailyMatches) {
		this.numberOfDailyMatches = numberOfDailyMatches;
	}

	public Integer getLeagueChampion() {
		return leagueChampion;
	}

	public void setLeagueChampion(Integer leagueChampion) {
		this.leagueChampion = leagueChampion;
	}

	public Date getLeagueStartDate() {
		return leagueStartDate;
	}

	public void setLeagueStartDate(Date leagueStartDate) {
		this.leagueStartDate = leagueStartDate;
	}
			
}
