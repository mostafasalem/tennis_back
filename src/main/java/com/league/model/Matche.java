package com.league.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "matches")
@NamedQueries(
		@NamedQuery(name = "Matche.findFirstRoundMatches", query = "select m from Matche m inner join Round r where r.roundNumber = 1")
)
public class Matche implements Serializable {
	/** Version identifier for serialization. */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "matche_id")
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player1", referencedColumnName = "participant_id")
	private Participant player1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player2", referencedColumnName = "participant_id")
	private Participant player2;
	
	@Column(name = "result")
	private String result;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "winner", referencedColumnName = "participant_id")
	private Participant winner;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROUND_ID", referencedColumnName = "ROUND_ID")
	private Round round;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "LEAGUE_ID", referencedColumnName = "LEAGUE_ID")
	private League league;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Participant getPlayer1() {
		return player1;
	}

	public void setPlayer1(Participant player1) {
		this.player1 = player1;
	}

	public Participant getPlayer2() {
		return player2;
	}

	public void setPlayer2(Participant player2) {
		this.player2 = player2;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Participant getWinner() {
		return winner;
	}

	public void setWinner(Participant winner) {
		this.winner = winner;
	}

	public Round getRound() {
		return round;
	}

	public void setRound(Round round) {
		this.round = round;
	}

	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}
	
	

}
