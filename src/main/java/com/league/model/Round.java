package com.league.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "rounds")
public class Round implements Serializable {
	/** Version identifier for serialization. */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "round_id")
	private Integer id;

	@Column(name = "round_number")
	private Integer roundNumber;

	@Column(name = "closed")
	private Boolean closed;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "LEAGUE_ID", referencedColumnName = "LEAGUE_ID")
	private League league;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "round_winner", referencedColumnName = "participant_id")
	private Participant roundWinner;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "round", cascade = CascadeType.ALL)
	private List<Matche> matches;


	public List<Matche> getMatches() {
		return matches;
	}

	public void setMatchs(List<Matche> matches) {
		this.matches = matches;
	}

	public void setMatches(List<Matche> matches) {
		this.matches = matches;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoundNumber() {
		return roundNumber;
	}

	public void setRoundNumber(Integer roundNumber) {
		this.roundNumber = roundNumber;
	}

	public Boolean isClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public Participant getRoundWinner() {
		return roundWinner;
	}

	public void setRoundWinner(Participant roundWinner) {
		this.roundWinner = roundWinner;
	}

	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}
	
	

}
