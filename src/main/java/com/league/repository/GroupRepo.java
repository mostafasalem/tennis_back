package com.league.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.league.model.Group;

public interface GroupRepo extends JpaRepository<Group, Integer> {

}
