package com.league.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.league.model.League;


public interface LeagueRepo extends JpaRepository<League, Integer> {

}
