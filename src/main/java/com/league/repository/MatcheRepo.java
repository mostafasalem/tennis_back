package com.league.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.league.model.Matche;
import com.league.model.Round;



public interface MatcheRepo extends JpaRepository<Matche, Integer> {

	public Optional<List<Matche>> getByRound(Round round);
	
}
