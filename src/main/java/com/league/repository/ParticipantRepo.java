package com.league.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.league.model.Participant;


public interface ParticipantRepo extends JpaRepository<Participant, Integer> {

}
