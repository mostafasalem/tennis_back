package com.league.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.league.model.Round;


public interface RoundRepo extends JpaRepository<Round, Integer> {

	public Optional<Round> findByRoundNumber(int number);
	
}
