package com.league.response;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.league.exception.ResourceNotFoundException;

/**
 * 
 * @author Mostafa Salem
 * 
 */

public class Response<T> {
	private HttpStatus status;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;

	private T data;

	private Response() {
		this.timestamp = LocalDateTime.now();
	}

	public Response(T type, HttpStatus status) throws ResourceNotFoundException {
		this();
		if (type == null || (type instanceof List && ((List) type).isEmpty())) {
			throw new ResourceNotFoundException("No Content Found");
		}
		this.status = status;
		this.data = type;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
