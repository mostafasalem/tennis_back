package com.league.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Mostafa Salem
 * 
 */

public class ResponseWrapper<T> extends ResponseEntity<T> {

	@SuppressWarnings("unchecked")
	public ResponseWrapper(T t, HttpStatus status) {
		super((T) new Response<>(t, status), status);
	}

}
