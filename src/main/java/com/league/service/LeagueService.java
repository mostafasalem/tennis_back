package com.league.service;

import com.league.dto.LeagueDTO;
import com.league.model.League;

public interface LeagueService {

	public LeagueDTO addLeagueChanmpion(LeagueDTO leagueDTO) throws Exception;
	public League validateLeague(int leagueId) throws Exception;
	
}
