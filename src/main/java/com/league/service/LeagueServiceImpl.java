package com.league.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.league.dto.LeagueDTO;
import com.league.dto.ParticipantDTO;
import com.league.exception.ResourceNotFoundException;
import com.league.model.League;
import com.league.model.Matche;
import com.league.model.Participant;
import com.league.repository.LeagueRepo;
import com.league.repository.MatcheRepo;
import com.league.repository.ParticipantRepo;

import javassist.NotFoundException;

@Service
@Transactional
public class LeagueServiceImpl  implements LeagueService {

	@Autowired
	private LeagueRepo leagueRepo;
	
	@Autowired
	private ParticipantService participantSrc;
	
	@Autowired
    private ModelMapper modelMapper;
	

	@Override
	public LeagueDTO addLeagueChanmpion(LeagueDTO leagueDTO) throws Exception {
		try {
			
			Participant participant = participantSrc.validateParticipant(leagueDTO.getLeagueChampion());
			League league = validateLeague(leagueDTO.getId());
			
			league.setLeagueChampion(participant.getId());
			League savedLeague = leagueRepo.save(league);
			return convertToDto(savedLeague);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public League validateLeague(int leagueId) throws Exception {
		Optional<League> league = leagueRepo.findById(leagueId);
		if (!league.isPresent()) {	
			throw new ResourceNotFoundException("Exception League not found");
		}	
		return league.get();
	}

	
	private LeagueDTO convertToDto(League league) {
		LeagueDTO leagueDTO = modelMapper.map(league, LeagueDTO.class);
	    return leagueDTO;
	}

	
	
}
