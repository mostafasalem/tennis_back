package com.league.service;

import java.util.List;

import com.league.dto.MatcheDTO;
import com.league.model.Matche;

public interface MatcheService {

	public MatcheDTO updateMatcheWinner(MatcheDTO matcheDTO) throws Exception;
	public MatcheDTO addMatche(MatcheDTO matcheDTO) throws Exception ;
	public Matche validateMatche(int matcheId) throws Exception ;
	public List<MatcheDTO> getFirstRoundMatches() throws Exception ;
	
}
