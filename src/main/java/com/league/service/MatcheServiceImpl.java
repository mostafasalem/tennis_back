package com.league.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.league.dto.MatcheDTO;
import com.league.dto.ParticipantDTO;
import com.league.exception.ResourceNotFoundException;
import com.league.model.Matche;
import com.league.model.Participant;
import com.league.model.Round;
import com.league.repository.MatcheRepo;
import com.league.repository.RoundRepo;


@Service
@Transactional
public class MatcheServiceImpl implements MatcheService {

	
	@Autowired
	private MatcheRepo matcheRepo;
	
	@Autowired
	private RoundRepo roundRepo;
	
	@Autowired
	private ParticipantService participantSrc;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public MatcheDTO updateMatcheWinner(MatcheDTO matcheDTO) throws Exception {
		try {
			Matche matche = validateMatche(matcheDTO.getId());
			Participant winner = participantSrc.validateParticipant(matcheDTO.getWinner());
			
			matche.setWinner(winner);
			matche.setResult(matcheDTO.getResult());
			Matche savedMatche = matcheRepo.save(matche);
			return convertToDto(savedMatche);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public MatcheDTO addMatche(MatcheDTO matcheDTO) throws Exception {
		try {
			Participant p1 = participantSrc.validateParticipant(matcheDTO.getPlayer1());
			Participant p2 = participantSrc.validateParticipant(matcheDTO.getPlayer2());
			Matche matche = convertToEntity(matcheDTO);
			matche.setPlayer1(p1);
			matche.setPlayer2(p2);
			return convertToDto(matcheRepo.save(matche));
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<MatcheDTO> getFirstRoundMatches() throws Exception {
		try {
			Optional<Round> round = roundRepo.findByRoundNumber(1);
			Optional<List<Matche>> matches = matcheRepo.getByRound(round.get());
			if (matches.isPresent()) {	
				List<MatcheDTO> allM = matches.get().stream()
						.map(m -> convertToDto(m))
						.collect(Collectors.toList());
				
				return allM;
			}
			return null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Matche validateMatche(int matcheId) throws Exception {
		Optional<Matche> matche = matcheRepo.findById(matcheId);
		if (!matche.isPresent()) {	
			throw new ResourceNotFoundException("Exception Matche not found");
		}
		return matche.get();
	}
	
	private ModelMapper getMapper() {
		// custom mapping
		PropertyMap<Matche, MatcheDTO> matcheMap = new PropertyMap<Matche, MatcheDTO>() {
		      protected void configure() {
		        map().setPlayer1(source.getPlayer1().getId());
		        map().setPlayer2(source.getPlayer2().getId());
		        map().setWinner(source.getWinner().getId());
		      }
		    };
	    TypeMap<Matche, MatcheDTO> typeMap = modelMapper.getTypeMap(Matche.class, MatcheDTO.class);
	    if (typeMap == null) { 
	       modelMapper.addMappings(matcheMap);
	    }
	    return modelMapper;
	}
	
	private Matche convertToEntity(MatcheDTO matcheDTO) {
		Matche matche = getMapper().map(matcheDTO, Matche.class);
	    return matche;
	}
	
	private MatcheDTO convertToDto(Matche matche) {
		MatcheDTO matcheDTO = getMapper().map(matche, MatcheDTO.class);
	    return matcheDTO;
	}


}
