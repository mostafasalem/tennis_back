package com.league.service;

import java.util.List;
import java.util.Map;

import com.league.dto.ParticipantDTO;
import com.league.model.Participant;

public interface ParticipantService {

	
	public ParticipantDTO addParticipantToLeague(ParticipantDTO participantDTO) throws Exception ;
	public List<ParticipantDTO> getAllParticipants() throws Exception ;
	public Participant validateParticipant(int winnerId) throws Exception;
	public Map<Integer, List<Integer>> groupParticipants(int groups, int league) throws Exception;
	
}
