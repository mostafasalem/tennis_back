package com.league.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.league.dto.LeagueDTO;
import com.league.dto.MatcheDTO;
import com.league.dto.ParticipantDTO;
import com.league.exception.ResourceNotFoundException;
import com.league.model.Group;
import com.league.model.League;
import com.league.model.Matche;
import com.league.model.Participant;
import com.league.repository.GroupRepo;
import com.league.repository.LeagueRepo;
import com.league.repository.ParticipantRepo;

@Service
@Transactional
public class ParticipantServiceImpl implements ParticipantService {
	
	@Autowired
	private ParticipantRepo participantRepo;
	
	@Autowired
	private GroupRepo groupRepo;
	
	@Autowired
	private LeagueRepo leagueRepo;
	
	@Autowired
	private LeagueService leagueSrc;

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public ParticipantDTO addParticipantToLeague(ParticipantDTO participantDTO) throws Exception  {
		try {
			League league = leagueSrc.validateLeague(participantDTO.getLeagueId());
			Participant p = convertToEntity(participantDTO);
			participantRepo.save(p);
			return participantDTO;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<ParticipantDTO> getAllParticipants() throws Exception  {
		try {
			List<Participant> parts = participantRepo.findAll();
			List<ParticipantDTO> allP = parts.stream()
					.map(p -> convertToDto(p))
					.collect(Collectors.toList());
			
			return allP;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Participant validateParticipant(int participantId) throws Exception {
		Optional<Participant> participant = participantRepo.findById(participantId);
		if (!participant.isPresent()) {	
			throw new ResourceNotFoundException("Exception Participant not found");
		}
		return participant.get();
	}
	
	public Map<Integer, List<Integer>> groupParticipants(int groups, int league) {
		List<Participant> parts = participantRepo.findAll();
		Map<Integer, List<Integer>> nGroups = 
				parts.stream()
				.map(p -> p.getId())
				.collect(Collectors.groupingBy(s -> (s - 1) / groups));
		
		Optional<League> firstLeague = leagueRepo.findById(league);
		
		
		for (Map.Entry<Integer, List<Integer>> entry : nGroups.entrySet()) {
			Group g = new Group();
			if(firstLeague.isPresent()) {
				g.setLeague(firstLeague.get());
			}
			g.setNumberOfPlayers(entry.getValue().size());
			g.setName("group_"+entry.getKey());
			Group newGroup = groupRepo.save(g);
			for(Integer pId : entry.getValue()) {
				Optional<Participant> participant = participantRepo.findById(pId);
				if(participant.isPresent()) {
					participant.get().setGroup(newGroup);
					participantRepo.save(participant.get());
				}
			}
		}
		
		return nGroups;
	}

	private Participant convertToEntity(ParticipantDTO participantDTO) {
		Participant participant = modelMapper.map(participantDTO, Participant.class);
	    return participant;
	}
	
	
	private ParticipantDTO convertToDto(Participant participant) {
		ParticipantDTO participantDTO = modelMapper.map(participant, ParticipantDTO.class);
	    return participantDTO;
	}
	

}
