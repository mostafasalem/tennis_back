package com.league.service;

import com.league.dto.RoundDTO;
import com.league.model.Round;

public interface RoundService {

	public RoundDTO closeRound(RoundDTO roundDTO) throws Exception ;
	public Round validateRound(int roundId) throws Exception;
	public RoundDTO createRound(RoundDTO roundDTO) throws Exception ;
}
