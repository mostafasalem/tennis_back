package com.league.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.league.dto.LeagueDTO;
import com.league.dto.MatcheDTO;
import com.league.dto.RoundDTO;
import com.league.exception.ResourceNotFoundException;
import com.league.model.Matche;
import com.league.model.Participant;
import com.league.model.Round;
import com.league.repository.ParticipantRepo;
import com.league.repository.RoundRepo;

@Service
@Transactional
public class RoundServiceImpl implements RoundService {

	@Autowired
	private ParticipantService participantSrc;
	
	@Autowired
	private RoundRepo roundRepo;
	
	@Autowired
	private LeagueService leagueSrc;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public RoundDTO closeRound(RoundDTO roundDTO) throws Exception {
		try {
			Round round = validateRound(roundDTO.getId());
			Participant winner = participantSrc.validateParticipant(roundDTO.getRoundWinnerId());
			round.setRoundWinner(winner);
			round.setClosed(roundDTO.isClosed());
			Round savedRound = roundRepo.save(round);
			return convertToDto(savedRound);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public Round validateRound(int roundId) throws Exception {
		Optional<Round> round = roundRepo.findById(roundId);
		if (!round.isPresent()) {	
			throw new ResourceNotFoundException("Exception round not found");
		}
		return round.get();
	}
	
	private ModelMapper getMapper() {
		PropertyMap<Round, RoundDTO> roundMap = new PropertyMap<Round, RoundDTO>() {
		      protected void configure() {
		        map().setRoundWinnerId(source.getRoundWinner().getId());
		      }
		    };
	    TypeMap<Round, RoundDTO> typeMap = modelMapper.getTypeMap(Round.class, RoundDTO.class);
	    if (typeMap == null) { 
	       modelMapper.addMappings(roundMap);
	    }
	    return modelMapper;
	}
	
	private Round convertToEntity(RoundDTO roundDTO) {
		Round round = getMapper().map(roundDTO, Round.class);
	    return round;
	}
	
	private RoundDTO convertToDto(Round round) {
		RoundDTO roundDTO = getMapper().map(round, RoundDTO.class);
	    return roundDTO;
	}

	@Override
	public RoundDTO createRound(RoundDTO roundDTO) throws Exception {
		try {
			Round round = convertToEntity(roundDTO);
			leagueSrc.validateLeague(roundDTO.getLeagueId());
			Round savedRound = roundRepo.save(round);
			return convertToDto(savedRound);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
