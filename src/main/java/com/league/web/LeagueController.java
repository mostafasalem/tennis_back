package com.league.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.league.dto.LeagueDTO;
import com.league.response.ResponseWrapper;
import com.league.service.LeagueService;


@Validated
@RestController
@RequestMapping("/api/league")
@CrossOrigin(origins="http://localhost:4200")
public class LeagueController {
	
	@Autowired
	private LeagueService leagueService;	
	
	
	@PostMapping("/submitLeagueChampion")
	public ResponseWrapper<LeagueDTO> submitLeagueChampion(
			@RequestBody(required = true) LeagueDTO leagueDTO) throws Exception {
		
		LeagueDTO league  = leagueService.addLeagueChanmpion(leagueDTO);
				
		return new ResponseWrapper<LeagueDTO>(league, HttpStatus.OK);	
	}

}
