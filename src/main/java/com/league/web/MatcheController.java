package com.league.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.league.dto.MatcheDTO;
import com.league.response.ResponseWrapper;
import com.league.service.MatcheService;


@Validated
@RestController
@RequestMapping("/api/matche")
@CrossOrigin(origins="http://localhost:4200")
public class MatcheController {
	
	@Autowired
	private MatcheService matcheService;	
	
	
	@PutMapping("/updateMatcheWinner")
	public ResponseWrapper<MatcheDTO> updateMatcheWinner(
			@RequestBody(required = true) MatcheDTO matcheDTO) throws Exception {
		
		MatcheDTO matche  = matcheService.updateMatcheWinner(matcheDTO);
				
		return new ResponseWrapper<MatcheDTO>(matche, HttpStatus.OK);	
	}
	
	@PostMapping("/newMatche")
	public ResponseWrapper<MatcheDTO> newMatche(
			@RequestBody(required = true) MatcheDTO matcheDTO) throws Exception {
		
		MatcheDTO matche  = matcheService.addMatche(matcheDTO);
				
		return new ResponseWrapper<MatcheDTO>(matche, HttpStatus.OK);	
	}
	
	@GetMapping("/firstRoundMatches")
	public ResponseWrapper<List<MatcheDTO>> firstRoundMatches() throws Exception {
		
		List<MatcheDTO> matche  = matcheService.getFirstRoundMatches();
				
		return new ResponseWrapper<List<MatcheDTO>>(matche, HttpStatus.OK);	
	}

}
