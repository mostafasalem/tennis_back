package com.league.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.league.dto.MatcheDTO;
import com.league.dto.ParticipantDTO;
import com.league.model.Participant;
import com.league.response.ResponseWrapper;
import com.league.service.MatcheService;
import com.league.service.ParticipantService;


@Validated
@RestController
@RequestMapping("/api/participant")
@CrossOrigin(origins="http://localhost:4200")
public class ParticipantController {
	
	@Autowired
	private ParticipantService participantService;	
	
	
	@GetMapping("/getAllParticipants")
	public ResponseWrapper<List<ParticipantDTO>> getAllParticipants() throws Exception {
		
		List<ParticipantDTO> participants  = participantService.getAllParticipants();
				
		return new ResponseWrapper<List<ParticipantDTO>>(participants, HttpStatus.OK);	
	}
	
	@PostMapping("/addParticipant")
	public ResponseWrapper<ParticipantDTO> addParticipant(
			@RequestBody(required = true) ParticipantDTO participant) throws Exception {
		
		ParticipantDTO part  = participantService.addParticipantToLeague(participant);
				
		return new ResponseWrapper<ParticipantDTO>(part, HttpStatus.OK);	
	}
	
	@PostMapping("/groupParticipants/{groups}/{leagueId}")
	public ResponseWrapper<Map<Integer, List<Integer>>> groupParticipants(
			@PathVariable("groups") int numberOfGroups,
			@PathVariable("leagueId") int leagueId) throws Exception {
		
		Map<Integer, List<Integer>> groups  = participantService.groupParticipants(numberOfGroups, leagueId);
				
		return new ResponseWrapper<Map<Integer, List<Integer>>>(groups, HttpStatus.OK);	
	}

}
