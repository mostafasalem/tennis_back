package com.league.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.league.dto.LeagueDTO;
import com.league.dto.RoundDTO;
import com.league.response.ResponseWrapper;
import com.league.service.LeagueService;
import com.league.service.RoundService;


@Validated
@RestController
@RequestMapping("/api/round")
@CrossOrigin(origins="http://localhost:4200")
public class RoundController {
	
	@Autowired
	private RoundService roundService;	
	
	
	@PutMapping("/closeRound")
	public ResponseWrapper<RoundDTO> closeRound(
			@RequestBody(required = true) RoundDTO roundDTO) throws Exception {
		
		RoundDTO round  = roundService.closeRound(roundDTO);
				
		return new ResponseWrapper<RoundDTO>(round, HttpStatus.OK);	
	}
	
	@PostMapping("/createRound")
	public ResponseWrapper<RoundDTO> createRound(
			@RequestBody(required = true) RoundDTO roundDTO) throws Exception {
		
		RoundDTO round  = roundService.createRound(roundDTO);
				
		return new ResponseWrapper<RoundDTO>(round, HttpStatus.OK);	
	}

}
