package com.league;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import com.league.dto.ParticipantDTO;
import com.league.model.Participant;
import com.league.service.ParticipantService;
import com.league.web.ParticipantController;

@SpringBootTest
@TestPropertySource(
		  locations = "classpath:application.properties")
@AutoConfigureMockMvc
class LeagueApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ParticipantController participantController;
	
	@MockBean
	private ParticipantService service;

	
	@Test
	void contextLoads() {
		assertThat(participantController).isNotNull();
	}
	
	@Test
	public void getAllPlayers() throws Exception {
		
		this.mockMvc.perform(get("/api/participant/getAllParticipants")).andDo(print()).andExpect(status().isOk());
	}

}
